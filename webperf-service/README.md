## WebPerf Cloudrun service
An example service in NodeJS that accepts HTTP requests and runs web performance tests per URL submitted via Lighthouse and Puppeteer

### Set enviroment variables

```
export GCP_PROJECT="paack-end2end"
```

### Building and submitting to gcr.io

```
gcloud builds submit \
    --config=cloudbuild.yaml \
    --project=$GCP_PROJECT \
    --substitutions=_WEBPERF_IMAGE="gcr.io/$GCP_PROJECT/webperf:latest"
```