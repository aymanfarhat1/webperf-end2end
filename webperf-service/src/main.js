const express = require('express');
const { PubSub } = require('@google-cloud/pubsub');
const audit = require('./audit');


const app = express();
app.use(express.raw());
app.use(express.json({ limit: '5mb', extended: true }));
app.use(express.urlencoded({ limit: '5mb', extended: true }))

app.post('/audit', runAndPublishAudit);
app.get('/', index)

async function index(req, res) {
  res.send('It is alive!');
}

async function runAndPublishAudit(req, res) {
  const payload = req.body;
  const url = payload.url;
  if (typeof (url) != 'undefined') {
    const page = await audit.getBrowserPage();
    const result = await audit.performAudit(url, page);
    const formatted = audit.formatResults(result);

    /** Exercise start */
    const topic = process.env.PUBSUB_TOPIC;
    // TODO: Publish to pubsub + Add check on the validity of the topic...
    to_publish = {
      'url': formatted['url'],
      'date': formatted['date'],
      'metrics': JSON.stringify(formatted)
    }

    const messageId = publishToPubsubTopic(to_publish, topic);
    /** Exercise end */

    res.json({
      'status': 'success',
      'messageId': messageId,
      'result': formatted
    });
  } else {
    // TODO: Return an exception
    res.json({});
  }
}

/** Exercise start */
/**
 * 
 * @param {*} payload 
 * @param {*} topicName 
 * @returns 
 */
async function publishToPubsubTopic(payload, topicName) {
  const pubSubClient = new PubSub();
  const dataBuffer = Buffer.from(JSON.stringify(payload));
  // TODO Add a try catch here...
  const messageId = await pubSubClient
    .topic(topicName)
    .publishMessage({ data: dataBuffer });

  return messageId;
}
/** Exercise end */

module.exports = app
