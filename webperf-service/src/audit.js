const dateFormat = require('date-format');
const lighthouse = require('lighthouse');
const puppeteer = require('puppeteer');
const chromeLauncher = require('chrome-launcher');


const fieldMapping = {
    'firstContentfulPaint': 'first-contentful-paint',
    'largestContentfulPaint': 'largest-contentful-paint',
    'firstMeaningfulPaint': 'first-meaningful-paint',
    'speedIndex': 'speed-index',
    'totalBlockingTime': 'total-blocking-time',
    'maxPotentialFID': 'max-potential-fid',
    'cumulativeLayoutShift': 'cumulative-layout-shift',
    'interactive': 'interactive',
    'serverResponseTime': 'server-response-time',
};


/**
 * Launches a local headless browser and returns a new page object
 * @returns PageObject
 */
async function getBrowserPage() {
    const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox'],
    });

    return browser.newPage();
}

/**
 * Takes a target url and a page instance from the local chromium browser
 * runs a lighthouse web audit on the supplied URL. Selected audits are 
 * performanced based on the field mapping object in this module.
 * @param {String} url 
 * @param {PageObject} page 
 * @returns 
 */
async function performAudit(url, page) {
    const port = page.browser().wsEndpoint().split(':')[2].split('/')[0];
    const options = {
        port,
    };

    const auditConfig = {
        extends: 'lighthouse:default',
        settings: {
            onlyAudits: [
                'first-contentful-paint',
                'largest-contentful-paint',
                'first-meaningful-paint',
                'speed-index',
                'estimated-input-latency',
                'total-blocking-time',
                'max-potential-fid',
                'cumulative-layout-shift',
                'first-cpu-idle',
                'interactive',
                'server-response-time',
            ],
        }
    }

    return await lighthouse(url, options, auditConfig)
        .then((metrics) => {
            const audits = metrics.lhr.audits;

            if (typeof (audits) != 'undefined' && audits != null) {
                audits['url'] = url;
                return audits;
            }
        }).catch((e) => {
            throw new Error(`LH Audit error: ${e.message}`);
        });
}

/**
 * Takes in a raw lighthouse result object, applies extration and formatting
 * based on the local fieldMapping configuration of this module
 * @param {Object} lighthouseAudit 
 * @returns Object
 */
function formatResults(lighthouseAudit) {
    const formattedResults = Object.entries(fieldMapping)
        .reduce((res, keyVal) => {
            res[keyVal[0]] = lighthouseAudit[keyVal[1]].numericValue;
            return res;
        }, {})

    formattedResults['date'] = dateFormat(
        'yyyy-MM-dd hh:mm:ss',
        new Date()
    )

    formattedResults['url'] = lighthouseAudit.url;

    return formattedResults;
}

module.exports = {
    formatResults,
    performAudit,
    getBrowserPage
}