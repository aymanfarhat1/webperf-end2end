gcloud builds submit \
    --config=cloudbuild.yaml \
    --project=$GCP_PROJECT \
    --substitutions=_WEBPERF_IMAGE="gcr.io/$GCP_PROJECT/webperf:latest"
