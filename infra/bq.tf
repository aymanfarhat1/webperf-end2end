resource "google_bigquery_dataset" "pubsub_dataset" {
  project       = var.project_id
  dataset_id    = "webperf"
  friendly_name = "webperf"
  description   = "PubSub to BigQuery example dataset"
  location      = "EU"

  delete_contents_on_destroy = true
  #access {
  #  role          = "OWNER"
  #  user_by_email = var.service_account_email
  #}

  #access {
  #  role          = "OWNER"
  #  user_by_email = "aymanf@google.com"
  #}
}

resource "google_bigquery_table" "perf_clean_data" {
  project             = var.project_id
  dataset_id          = google_bigquery_dataset.pubsub_dataset.dataset_id
  table_id            = "perf_clean_data"
  deletion_protection = false
  schema              = <<EOF
[
  {
    "name": "url",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "date",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "metrics",
    "type": "STRING",
    "mode": "NULLABLE"
  }
]
EOF
}

resource "google_bigquery_table" "perf_raw_data" {
  project             = var.project_id
  dataset_id          = google_bigquery_dataset.pubsub_dataset.dataset_id
  table_id            = "perf_raw_data"
  deletion_protection = false
  schema              = <<EOF
[
  {
    "name": "data",
    "type": "STRING",
    "mode": "NULLABLE"
  }
]
EOF
}

resource "google_project_iam_member" "viewer" {
  project = var.project_id
  role    = "roles/bigquery.metadataViewer"
  member  = "serviceAccount:service-${var.project_number}@gcp-sa-pubsub.iam.gserviceaccount.com"
}

resource "google_project_iam_member" "editor" {
  project = var.project_id
  role    = "roles/bigquery.dataEditor"
  member  = "serviceAccount:service-${var.project_number}@gcp-sa-pubsub.iam.gserviceaccount.com"
}