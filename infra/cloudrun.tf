resource "google_cloud_run_service" "webperf" {
  project  = var.project_id
  name     = "webperf-service"
  location = "europe-west1"
  provider = google-beta
  template {
    spec {
      containers {
        image = "gcr.io/${var.project_id}/webperf:latest"
        env {
          name  = "PUBSUB_TOPIC"
          value = "webperf_result"
        }
        resources {
          limits = {
            cpu    = "2.0"
            memory = "8000Mi"
          }
        }
      }
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
  metadata {
    annotations = {
      "run.googleapis.com/ingress" = "internal"
    }
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location = google_cloud_run_service.webperf.location
  project  = google_cloud_run_service.webperf.project
  service  = google_cloud_run_service.webperf.name

  policy_data = data.google_iam_policy.noauth.policy_data
}