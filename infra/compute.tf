resource "google_compute_instance" "default" {
  project                   = var.project_id
  name                      = "dev-machine"
  machine_type              = "n2-standard-2"
  zone                      = "europe-west2-a"
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      size  = 100
      type  = "pd-ssd"
    }
  }

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "TERMINATE"
  }

  metadata_startup_script = file("${path.module}/scripts/init.sh")
  service_account {
    email  = var.service_account_email
    scopes = ["cloud-platform"]
  }

  network_interface {
    network = google_compute_network.default.name

    access_config {
      // Ephemeral public IP
    }
  }
}