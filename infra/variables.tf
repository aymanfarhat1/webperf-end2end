variable "project_id" {
  description = "Project ID"
  type        = string
}

variable "project_number" {
  description = "Project Number"
  type        = string
}

variable "service_account_email" {
  description = "Default development service account"
  type        = string
}

variable "prefix" {
  description = "Prefix to be added to all resource names."
  type        = string
  default     = "demo"
}

variable "subnet_cidr_range" {
  description = "CIDR range for the subnet."
  type        = string
  default     = "10.0.0.0/16"
}

variable "pod_cidr_range" {
  description = "CIDR range for the pods."
  type        = string
  default     = "10.100.0.0/16"
}

variable "service_cidr_range" {
  description = "CIDR range for the services."
  type        = string
  default     = "10.200.0.0/16"
}

variable "region" {
  description = "Region where to create the resources."
  type        = string
  default     = "europe-west1"
}

variable "zone" {
  description = "Zone where to create the resources."
  type        = string
  default     = "europe-west1-b"
}