resource "random_string" "queue_id" {
  length = 6
  lower  = true
  numeric=false
  special=false
}

resource "google_cloud_tasks_queue" "perf_tasks" {
  project = var.project_id
  name = "perf-queue-${random_string.queue_id.result}"
  location = var.region 
}