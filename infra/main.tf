locals {
  project_services = [
    "compute.googleapis.com",
    "container.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "containerregistry.googleapis.com",
    "cloudbuild.googleapis.com",
    "storage.googleapis.com",
    "artifactregistry.googleapis.com",
    "sourcerepo.googleapis.com",
    "iap.googleapis.com",
    "bigquery.googleapis.com",
    "run.googleapis.com",
    "cloudtasks.googleapis.com",
    "tasks.googleapis.com"
  ]
}

resource "google_project_service" "default" {
  for_each                   = toset(local.project_services)
  project                    = var.project_id
  service                    = each.value
  disable_dependent_services = false
  disable_on_destroy         = false
}

resource "google_compute_network" "default" {
  project = var.project_id
  name    = "webperf-dev-network"
}

resource "google_compute_firewall" "default" {
  project = var.project_id
  name    = "webperf-dev-firewall"
  network = google_compute_network.default.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8081", "8080", "22"]
  }

  source_tags   = ["web"]
  source_ranges = ["0.0.0.0/0"]
}