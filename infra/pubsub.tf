resource "google_pubsub_schema" "webperf_schema" {
  project                     = var.project_id
  name = "webperf_schema"
  type = "AVRO"
  definition = "{\n  \"type\" : \"record\",\n  \"name\" : \"Avro\",\n  \"fields\" : [\n {\n  \"name\" : \"url\",\n  \"type\" : \"string\"\n },\n    {\n  \"name\" : \"date\",\n  \"type\" : \"string\"\n    },\n  {\n  \"name\" : \"metrics\",\n  \"type\" : \"string\"\n    }\n  ]\n}\n"
}

resource "google_pubsub_topic" "webperf_result" {
  project = var.project_id
  name    = "webperf_result"
  depends_on = [google_pubsub_schema.webperf_schema]
  schema_settings {
    schema = google_pubsub_schema.webperf_schema.id
    encoding = "JSON"
  }
}

resource "google_pubsub_subscription" "webperf_result_sub" {
  project = var.project_id
  name  = "webperf_result_sub"
  topic = google_pubsub_topic.webperf_result.name
  bigquery_config {
    table = "${google_bigquery_table.perf_clean_data.project}:${google_bigquery_table.perf_clean_data.dataset_id}.${google_bigquery_table.perf_clean_data.table_id}"
    use_topic_schema = true
  }

  depends_on = [google_project_iam_member.viewer, google_project_iam_member.editor]
}