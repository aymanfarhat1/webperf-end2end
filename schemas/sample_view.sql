CREATE OR REPLACE TABLE `project.webperf.sub` AS
(SELECT
  url,
  PARSE_DATETIME('%Y-%m-%d %H:%M:%S', date) as test_timing,
  JSON_EXTRACT(metrics, '$.firstContentfulPaint')  as fcp,
  JSON_EXTRACT(metrics, '$.largestContentfulPaint') as lcp,
  JSON_EXTRACT(metrics, '$.speedIndex') as speed,
  JSON_EXTRACT(metrics, '$.serverResponseTime') as responseTime
FROM
  `paack-end2end.webperf.perf_clean_data`
WHERE url LIKE '%amazon%')

