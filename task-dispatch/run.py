import json
from google.cloud import tasks_v2

PROJECT_ID = 'paack-end2end'
LOCATION_ID = 'europe-west1'
QUEUE_ID = 'perf-queue-gwstzr'
SERVICE_URL = ''

"""Exercise start"""
def create_webperf_task(url_to_test):
    client = tasks_v2.CloudTasksClient()
    parent = client.queue_path(PROJECT_ID, LOCATION_ID, QUEUE_ID)
    service_url = f'https://{SERVICE_URL}/audit'
    payload = { 'url': url_to_test}
    task = {
        'http_request': {
            'http_method': tasks_v2.HttpMethod.POST,
            'url': service_url,
            'headers': {'Content-type': 'application/json'},
            'body': json.dumps(payload).encode()
        }
    }

    response = client.create_task({
        'parent': parent,
        'task': task
    })

with open('urls.txt') as source_file:
    for line in source_file:
        stripped = line.strip()

        if len(stripped) > 0:
            create_webperf_task(stripped)
            print(f'Dispatched {stripped}')

"""Exercise end"""
